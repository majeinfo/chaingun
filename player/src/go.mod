module github.com/majeinfo/chaingun

go 1.16

require (
	github.com/JumboInteractiveLimited/jsonpath v0.0.0-20180321012328-6fcdcc9066b5
	github.com/Knetic/govaluate v3.0.0+incompatible
	github.com/MakeNowJust/heredoc v1.0.0
	github.com/armon/consul-api v0.0.0-20180202201655-eb2c6b5be1b6 // indirect
	github.com/bmizerany/perks v0.0.0-20141205001514-d9a9656a3a4b
	github.com/eclipse/paho.mqtt.golang v1.3.2
	github.com/elazarl/goproxy v0.0.0-20210110162100-a92cc753f88e
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gogo/protobuf v1.3.2
	github.com/golang/protobuf v1.5.1
	github.com/gorilla/websocket v1.4.2
	github.com/jhump/protoreflect v1.8.2
	github.com/magiconair/properties v1.8.5 // indirect
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	github.com/pelletier/go-toml v1.8.1 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/afero v1.6.0 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v1.1.3 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/viper v1.7.1 // indirect
	github.com/ugorji/go v1.1.4 // indirect
	github.com/xordataexchange/crypt v0.0.3-0.20170626215501-b2862e3d0a77 // indirect
	go.mongodb.org/mongo-driver v1.5.1
	golang.org/x/net v0.0.0-20210326220855-61e056675ecf
	golang.org/x/sys v0.0.0-20210330210617-4fbd30eecc44 // indirect
	golang.org/x/text v0.3.5 // indirect
	google.golang.org/grpc v1.36.1
	gopkg.in/ini.v1 v1.62.0 // indirect
	gopkg.in/xmlpath.v2 v2.0.0-20150820204837-860cbeca3ebc
	gopkg.in/yaml.v2 v2.4.0
)
